using Furion;
using NiuPi.Database;
using NiuPi.Middleware;
//坑一：引用类型查询数据库时必须初始化new一个实例，否则会报空对象，sqlsugar暂不支持
var builder = WebApplication.CreateBuilder(args).Inject();
//注入数据库
builder.Services.AddSqlsugarSetup();
builder.Services.AddJwt<JwtHandler>(enableGlobalAuthorize: true);
builder.Services.AddCorsAccessor();
builder.Services.AddControllers();
builder.Services.AddSwaggerGen(c =>
{
    var file = Path.Combine(AppContext.BaseDirectory, "NiuPi.xml");  // xml文档绝对路径
    var path = Path.Combine(AppContext.BaseDirectory, file); // xml文档绝对路径
    c.IncludeXmlComments(path, true); // true : 显示控制器层注释
    c.OrderActionsBy(o => o.RelativePath); // 对action的名称进行排序，如果有多个，就可以看见效果了。
});
//获取请求者信息
builder.Services.AddRemoteRequest();
if (App.Configuration["Cache:CacheType"] == "RedisCache")
{
    builder.Services.AddStackExchangeRedisCache(options =>
    {
        options.Configuration = App.Configuration["Cache:RedisConnectionString"]; // redis连接配置
        options.InstanceName = App.Configuration["Cache:InstanceName"]; // 键名前缀
    });
}
//注册虚拟文件系统服务
builder.Services.AddVirtualFileServer();
builder.Services.AddInject();
var app = builder.Build();
app.UseHttpsRedirection();
//(鉴权)身份认证，明确是你谁，确认是不是合法用户。常用的认证方式有用户名密码认证。
app.UseAuthentication();
//(授权)明确你是否有某个权限。当用户需要使用某个功能的时候，系统需要校验用户是否需要这个功能的权限。
app.UseAuthorization();
app.UseCorsAccessor();
app.UseSwagger();
//如果 app.UseInject() 不输入参数，则默认地址为 /api，如果输入 string.Empty 则为 / 目录。如果输入任意字符串，则为 /任意字符串 目录
//UseInject 加载Furion框架
app.UseInject();
app.MapControllers();
app.Run();