﻿using Furion;
using SqlSugar;
using System.Reflection;

namespace NiuPi.Database
{
    public class RepositoryBase<T> : SimpleClient<T> where T : class, new()
    {
        protected ITenant? itenant = null;//多租户分库，暂时用一个库
        public RepositoryBase(SqlSugarScope? context = null) : base(context)//注意这里要有默认值等于null
        {
            var configId = typeof(T)?.GetCustomAttribute<TenantAttribute>()?.configId;
            Context = App.GetService<SqlSugarScope>().GetConnection(configId);
        }
    }
}