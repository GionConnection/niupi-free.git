import request from '/@/utils/request';
export function getMenuTree(params: object) {
	return request({
		url: '/platform/menu/menu-tree',
		method: 'get',
		params,
	});
}
export function getRouteMenuTree(params: object) {
	return request({
		url: '/platform/menu/route-menu-tree',
		method: 'get',
		params,
	});
}
export function add(params: object) {
	return request({
		url: '/platform/menu',
		method: 'post',
		data: params
	});
}
export function edit(params: object) {
	return request({
		url: '/platform/menu/edit',
		method: 'post',
		data: params
	});
}
export function add_all(params: object) {
	return request({
		url: '/platform/menu/all',
		method: 'post',
		data: params
	});
}
export function changeStatus(params: object) {
	return request({
		url: '/platform/menu/change-status',
		method: 'post',
		data: params
	});
}