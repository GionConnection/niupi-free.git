import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import { store, key } from './store';
import { directive } from '/@/utils/directive';
import { globalComponentSize } from '/@/utils/componentSize';

import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import '/@/theme/index.scss';
import mitt from 'mitt';
//引入i18n组件
import I18n from "/@/lang/index";
//引入highlightjs高亮组件
import 'highlight.js/styles/atom-one-dark.css';
import 'highlight.js/lib/common';
import highlight from '@highlightjs/vue-plugin';
const app = createApp(App);
app
    .use(router)
    .use(store, key)
    .use(ElementPlus, { size: globalComponentSize })
    .use(I18n)
    .use(highlight)
    .mount('#app');
app.config.globalProperties.mittBus = mitt();
directive(app);
