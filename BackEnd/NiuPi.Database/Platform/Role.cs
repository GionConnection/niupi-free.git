﻿using NiuPi.Database.BasicModel;
using SqlSugar;

namespace NiuPi.Database.Platform
{
    [SugarTable("role")]
    public class Role : Base
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        [SugarColumn(Length = 255, ColumnDescription = "角色名称", IsNullable = false,UniqueGroupNameList = new string[] { "RoleName" })]
        public string? RoleName { get; set; }
        /// <summary>
        /// 关联权限集合
        /// 不关联数据库
        /// </summary>
        [SugarColumn(IsIgnore =true)]
        public List<RolePermission> Permissions { get; set; }
    }
}
