/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 8.0.12 : Database - niupi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`niupi` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `niupi`;

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `ParentID` bigint(20) NOT NULL DEFAULT '0' COMMENT '菜单父级ID',
  `Type` int(11) NOT NULL DEFAULT '0' COMMENT '菜单类型（0目录 1菜单 2按钮）',
  `MenuName` varchar(255) NOT NULL COMMENT '菜单名称',
  `RoutePath` varchar(255) NOT NULL COMMENT '前端路由',
  `Permission` varchar(255) NOT NULL COMMENT '前端权限标识',
  `AssemblyPath` varchar(255) NOT NULL COMMENT '前端组件路径',
  `Icon` varchar(255) NOT NULL COMMENT '菜单图标',
  `Sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `TenantId` bigint(20) NOT NULL DEFAULT '0' COMMENT '多租户ID',
  `CreatedTime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `UpdatedTime` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `CreatedUserId` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建者Id',
  `IsDeleted` int(11) NOT NULL DEFAULT '0' COMMENT '软删除标记',
  `Status` int(11) NOT NULL DEFAULT '0' COMMENT '基础状态',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `menu` */

insert  into `menu`(`Id`,`ParentID`,`Type`,`MenuName`,`RoutePath`,`Permission`,`AssemblyPath`,`Icon`,`Sort`,`TenantId`,`CreatedTime`,`UpdatedTime`,`CreatedUserId`,`IsDeleted`,`Status`) values 
(1,0,0,'首页','/home','home','/home','el-icon-s-home',0,0,1645636277,1645636277,0,0,0),
(2,0,0,'平台管理','/platform','platform','','el-icon-s-platform',0,0,1645636278,1645636278,0,0,0),
(4,2,1,'用户管理','/platform/user','platform.user','/platform/user/index','el-icon-user',97,0,1645636278,1645636278,0,0,0),
(5,2,1,'菜单管理','/platform/menu','platform.menu','/platform/menu/index','el-icon-menu',98,0,1645636278,1645636278,0,0,0),
(6,2,1,'角色管理','/platform/role','platform.role','/platform/role/index','el-icon-s-check',96,0,1645636278,1645636278,0,0,0);

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `RoleName` varchar(255) NOT NULL COMMENT '角色名称',
  `TenantId` bigint(20) NOT NULL DEFAULT '0' COMMENT '多租户ID',
  `CreatedTime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `UpdatedTime` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `CreatedUserId` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建者Id',
  `IsDeleted` int(11) NOT NULL DEFAULT '0' COMMENT '软删除标记',
  `Status` int(11) NOT NULL DEFAULT '0' COMMENT '基础状态',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `role` */

insert  into `role`(`Id`,`RoleName`,`TenantId`,`CreatedTime`,`UpdatedTime`,`CreatedUserId`,`IsDeleted`,`Status`) values 
(1,'超级管理员',0,1648218995,1648218995,0,0,0),
(2,'普通管理员',0,1649850162,1649850162,0,0,0);

/*Table structure for table `role_permission` */

DROP TABLE IF EXISTS `role_permission`;

CREATE TABLE `role_permission` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `RoleId` int(11) NOT NULL DEFAULT '0' COMMENT '角色关联ID',
  `PermissionName` varchar(255) NOT NULL COMMENT '权限菜单名称',
  `Permission` varchar(255) NOT NULL COMMENT '角色权限标识',
  `TenantId` bigint(20) NOT NULL DEFAULT '0' COMMENT '多租户ID',
  `CreatedTime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `UpdatedTime` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `CreatedUserId` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建者Id',
  `IsDeleted` int(11) NOT NULL DEFAULT '0' COMMENT '软删除标记',
  `Status` int(11) NOT NULL DEFAULT '0' COMMENT '基础状态',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=411 DEFAULT CHARSET=utf8;

/*Data for the table `role_permission` */

insert  into `role_permission`(`Id`,`RoleId`,`PermissionName`,`Permission`,`TenantId`,`CreatedTime`,`UpdatedTime`,`CreatedUserId`,`IsDeleted`,`Status`) values 
(396,1,'首页','home',0,1648218995,1648218995,0,0,0),
(397,1,'平台管理','platform',0,1648218995,1648218995,0,0,0),
(399,1,'菜单管理','platform.menu',0,1648218995,1648218995,0,0,0),
(400,1,'用户管理','platform.user',0,1648218995,1648218995,0,0,0),
(401,1,'角色管理','platform.role',0,1648218995,1648218995,0,0,0),
(410,2,'首页','home',0,1649850162,1649850162,0,0,0);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `AdminType` int(11) NOT NULL DEFAULT '0' COMMENT '是否超级管理员：0否，1是',
  `Account` varchar(255) NOT NULL COMMENT '账号',
  `Password` varchar(255) NOT NULL COMMENT '密码',
  `Phone` varchar(255) NOT NULL COMMENT '手机号',
  `NickName` varchar(255) DEFAULT NULL COMMENT '昵称',
  `Avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `Birthday` varchar(255) DEFAULT NULL COMMENT '生日',
  `Sex` int(11) NOT NULL DEFAULT '0' COMMENT '性别',
  `Email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `RoleId` bigint(20) NOT NULL DEFAULT '0' COMMENT '关联角色ID',
  `StoreId` bigint(20) NOT NULL DEFAULT '0' COMMENT '关联店铺ID',
  `LastLoginIP` varchar(255) DEFAULT NULL COMMENT '最后登录IP',
  `LastLoginTime` bigint(255) DEFAULT NULL COMMENT '最后登录时间',
  `TenantId` bigint(20) NOT NULL DEFAULT '0' COMMENT '多租户ID',
  `CreatedTime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `UpdatedTime` bigint(20) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `CreatedUserId` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建者Id',
  `IsDeleted` int(11) NOT NULL DEFAULT '0' COMMENT '软删除标记',
  `Status` int(11) NOT NULL DEFAULT '0' COMMENT '基础状态',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`Id`,`AdminType`,`Account`,`Password`,`Phone`,`NickName`,`Avatar`,`Birthday`,`Sex`,`Email`,`RoleId`,`StoreId`,`LastLoginIP`,`LastLoginTime`,`TenantId`,`CreatedTime`,`UpdatedTime`,`CreatedUserId`,`IsDeleted`,`Status`) values 
(1,0,'admin','14e1b600b1fd579f47433b88e8d85291','13000000000','甜蜜蜜','https://img0.baidu.com/it/u=3904827974,2084857142&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500',NULL,1,'490912587@qq.com',1,0,'0.0.0.1',1649850271,1,1645636278,1645636278,0,0,0),
(2,0,'test','14e1b600b1fd579f47433b88e8d85291','15000000000','甜蜜蜜-一号','https://img0.baidu.com/it/u=3904827974,2084857142&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500','2022-02-25T09:41:02.760Z',0,'',2,0,'0.0.0.1',1649779639,1,1645810886,1645810886,0,0,0),
(3,0,'furion','698d51a19d8a121ce581499d7b701668','18000000000','甜蜜蜜-二号','https://img0.baidu.com/it/u=3904827974,2084857142&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500','2022-02-28T03:29:15.119Z',0,'',2,0,'0.0.0.1',1649779639,1,1646047780,1646047780,0,0,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
